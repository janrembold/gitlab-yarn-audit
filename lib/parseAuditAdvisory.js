const unified = require('unified');
const markdown = require('remark-parse');
const visit = require('unist-util-visit');

function parseReferences(references) {
  const reports = [];
  const tree = unified()
    .use(markdown)
    .parse(references);
  visit(tree, 'link', ({ url }) => {
    reports.push({ url });
  });
  return reports;
}

module.exports = function parseAuditAdvisory(advisory) {
  return {
    confidence: 'High',
    description: advisory.overview,
    identifiers: advisory.cves
      .map(cve => ({
        name: cve,
        url: `https://nvd.nist.gov/vuln/detail/${cve}`,
      }))
      .concat({
        name: advisory.cwe,
        url: `https://cwe.mitre.org/data/definitions/${advisory.cwe.replace(/^CWE-/, '')}`,
      })
      .sort((a, b) => (a.name < b.name ? -1 : 1)),
    instances: []
      .concat(...advisory.findings.map(({ paths }) => paths.map(method => ({ method }))))
      .sort(),
    links: [{ url: advisory.url }]
      .concat(parseReferences(advisory.references))
      .sort((a, b) => (a.url < b.url ? -1 : 1)),
    location: {
      file: 'yarn.lock',
    },
    message: advisory.title,
    namespace: advisory.module_name,
    severity: Array.from(advisory.severity, (c, i) => (i ? c : c.toUpperCase())).join(''),
    solution: advisory.recommendation,
  };
};
