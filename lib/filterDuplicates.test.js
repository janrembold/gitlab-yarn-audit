const filterDuplicates = require('./filterDuplicates');

it('should filter duplicate entries by id', () => {
  const data = [{ id: 1 }, { id: 3 }, { id: 1 }, { id: 1 }, { id: 4 }, { id: 1 }];
  const result = data.filter(filterDuplicates());
  expect(result).toStrictEqual([{ id: 1 }, { id: 3 }, { id: 4 }]);
});
